﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal;
using FormModels = Dal.FormModels;

namespace Services
{
    public class LibraryService : IDisposable
    {
        private readonly LibraryDbContext _db;

        public LibraryService()
        {
            _db = new LibraryDbContext();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public List<FormModels.Library> GetLibrariesForDropDown()
        {
            return _db.Libraries.Select(m => new FormModels.Library
            {
                Id = m.Id,
                Name = m.Name,
                Details = m.Details,
                Address = m.Address
            }).ToList();
        }
    }
}
