﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Dal;
using FormModels = Dal.FormModels;
using MySql.Data.MySqlClient;

namespace Services
{
    public class LibrariesService : BaseService
    {
        private readonly LibraryDbContext _db;
        public LibrariesService()
        {
            _db = new LibraryDbContext();
        }

        public List<FormModels.Library> GetLibraries()
        {

            return _db.Libraries.Select(m => new FormModels.Library
            {
                Id = m.Id,
                Address = m.Address,
                Name = m.Name,
                Details = m.Details
            }).ToList();
        }


    }
}
