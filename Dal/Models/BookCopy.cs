﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class BookCopy
    {
        [Key]
        public string Isbn { get; private set; }

        public int BookId { get; private set; }

        public string Barcode { get; private set; }

        public int LibraryId { get; private set; }

        private BookCopy()
        {
        }

        public BookCopy(string isbn, int bookId, string barcode, int libraryId)
        {
            Isbn = isbn;
            BookId = bookId;
            Barcode = barcode;
            LibraryId = libraryId;
        }
    }
}
