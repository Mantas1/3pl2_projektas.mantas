﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("libraries")]
    public class Library
    {
        [Key]
        public int Id { get; private set; }

        public string Name { get; private set; }

        public string Address { get; private set; }

        public string Details { get; private set; }

        private Library() { }

        public Library(string name, string address, string details)
        {
            Name = name;
            Address = address;
            Details = details;
        }
    }
}
