﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.FormModels
{
    public class AddBookCopy
    {
        public int BookId { get; set; }

        public string Isbn { get; set; }

        public int Library { get; set; }
    }
}
