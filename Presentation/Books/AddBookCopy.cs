﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormModels = Dal.FormModels;
using Services;

namespace Presentation.Books
{
    public partial class AddBookCopy : Form
    {
        private readonly BooksService _bookService;
        private readonly LibraryService _libraryService;

        public AddBookCopy()
        {
        InitializeComponent();
            _libraryService = new LibraryService();
            _bookService = new BooksService();
            FillCombobox();
        }

        public void FillCombobox()
        {
            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "Name";

            cmbxBooks.ValueMember = "Id";
            cmbxBooks.DisplayMember = "Title";

            var libraries = _libraryService.GetLibrariesForDropDown();
            var books = _bookService.GetBooksForDropDown();

            comboBox1.Items.Clear();
            cmbxBooks.Items.Clear();

            comboBox1.DataSource = new BindingSource(libraries, null);
            cmbxBooks.DataSource = new BindingSource(books, null);

            comboBox1.SelectedItem = null;
            cmbxBooks.SelectedItem = null;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var bookCopy = new FormModels.AddBookCopy
            {
                Isbn = txtIsbn.Text,
                BookId = Convert.ToInt32(cmbxBooks.SelectedValue),
                Library = Convert.ToInt32(comboBox1.SelectedValue)
            };
        }

        private void btnGenerateBarcode_Click(object sender, EventArgs e)
        {
            var guid = Guid.NewGuid();
            txtIsbn.Text = guid.ToString();
        }
    }
}
