﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Books
{
    public partial class BookSearch : Form
    {
        private readonly BooksService _service;
        public BookSearch()
        {
            InitializeComponent();
            _service = new BooksService();
            FillGrid();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void FillGrid()
        {
            var data = _service.BookSearch();
            dataGridView1.Rows.Clear();

            foreach (var book in data)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["BookId"].Value = book.BookId;
                dataGridView1.Rows[rowIndex].Cells["Categories"].Value = book.Categories;
                dataGridView1.Rows[rowIndex].Cells["Title"].Value = book.Title;
                dataGridView1.Rows[rowIndex].Cells["Authors"].Value = book.Authors;
                dataGridView1.Rows[rowIndex].Cells["Quantity"].Value = book.Quantity;
                dataGridView1.Rows[rowIndex].Cells["DateOfPublish"].Value = book.DateOfPublish;
            }
        }

        private void btnSeekCategory_Click(object sender, EventArgs e)
        {
            CategorySelect categorySelect = new CategorySelect();
            categorySelect.FormClosed += CategorySelect_FormClosed;
            categorySelect.ShowDialog();

        }

        private void CategorySelect_FormClosed(object sender, FormClosedEventArgs e)
        {
            var list = ((CategorySelect)sender).CategoriesForSearch;
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            var data = _service.BookSearch();
        }

        private void BtnAddBook_Click(object sender, EventArgs e)
        {
            AddBook addBook = new AddBook();
            addBook.FormClosed += AddBook_FormClosed;
            addBook.ShowDialog();
        }

        private void AddBook_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
