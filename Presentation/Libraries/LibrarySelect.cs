﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.FormModels;
using Services;

namespace Presentation.Libraries
{
    public partial class LibrarySelect : Form
    {
        private readonly LibrariesService _service;

        public List<Library> LibrariesForSearch { get; set; } = new List<Library>();
        public LibrarySelect()
        {
            

            InitializeComponent();
            _service = new LibrariesService();
            FillGrid();
        }
        public void FillGrid()
        {
            var libraries = _service.GetLibraries();

            foreach (var library in libraries)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["Id"].Value = library.Id;
                dataGridView1.Rows[rowIndex].Cells["Name"].Value = library.Name;
                dataGridView1.Rows[rowIndex].Cells["Address"].Value = library.Address;
                dataGridView1.Rows[rowIndex].Cells["Details"].Value = library.Details;
            }
        }
    }
}
